$(document).ready(function() {
  //start of prototype function to find unique in array
      Array.prototype.contains = function(v) {
          for(var i = 0; i < this.length; i++) {
              if(this[i] === v) return true;
          }
          return false;
      };

      Array.prototype.unique = function() {
          var arr = [];
          for(var i = 0; i < this.length; i++) {
              if(!arr.contains(this[i])) {
                  arr.push(this[i]);
              }
          }
          return arr; 
      };
      //end of prototype function to find unique in array
});  

function populateGenre(){
  genreDupList = new Array();
  genreList = new Array();

  $('.movie-col h5').each(function(){
    var genreString = $(this).text().replace(/\s/g,"").replace(/-/g,"").toLowerCase();
    var filterTerm = genreString.split("|");
    $.each(filterTerm,function(i,el){
      genreDupList.push(el);
    });
  });
  genreList = genreDupList.unique().sort();
  $.each(genreList, function (index, value) {
    $('#genre-ul').append(`<li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="${value}"> ${value}</label></a></li>`); 
  });
}

$(document).ready(function(e){
  $('.search-panel .dropdown-menu').find('a').click(function(e) {
    e.preventDefault();
    var param = $(this).attr("href").replace("#","");
    var concept = $(this).text();
    $('.search-panel span#search_concept').text(concept);
    $('.input-group #search_param').val(param);
  });
});