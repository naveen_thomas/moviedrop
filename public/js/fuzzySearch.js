// start fuzzy search
    jQuery(document).ready(function($){
    
      $('#live-search-box').on('keyup', function(){ 

        $('.browse-movie-title').each(function(i,el){
          $(this).attr('data-search-term', $(this).text().toLowerCase());
        });
        
        var searchTerm = $(this).val().toLowerCase();
        $('.browse-movie-title').each(function(i,el){
          var genreSelected = $('#genre-multi-select option:selected');
          if(genreSelected.size() > 0){
            if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
                if($(el).parents('.movie-col').find("h5").attr("filter-flag") == "true"){
                    $(el).parents('.movie-col').show();
                }
            } else {
                $(el).parents('.movie-col').hide();
            }
          }else{
            if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
              $(el).parents('.movie-col').show();
            }else{
              $(el).parents('.movie-col').hide();
            }
          }
        });
      });
    });
// end fuzzy search