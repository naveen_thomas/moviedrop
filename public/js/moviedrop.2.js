$(function(){

	var WAIT_FOR_PARSER_TO_COMPLETE = 2 * 1000;
	var RAW_FILENAMES = [];
	var url_ids = [];
	var TMDB_API_KEY = '86384eee49223f8c4807c049d0bcc68f';
	var OMDB_API_KEY = '8bdd2293';
	var TMDB_CONCURRENT_REQUESTS = 40;
	var TMDB_CONCURRENT_REQUESTS_WINDOW_TIME = 15;
	var TMDB_URLS = [];
    var MOVIES = {};

	// Begin Drag Drop Handlers
	var dropZone = document.getElementById('drop-zone');
  
  dropZone.addEventListener('dragover', function(evt){
  	evt.stopPropagation();
    evt.preventDefault();
    // Explicitly show this is a copy.
    evt.dataTransfer.dropEffect = 'copy';
  }, false);

  function callDelay() {
	  $('#center-content').slideUp('slow', function(){
	    document.getElementById('loaderDiv').style.visibility='visible';
	    document.getElementById('loaderDiv').style.display='block';
	  });
	  setTimeout(showMovieGrid, 3000);
	}

	function showMovieGrid() {
	  document.getElementById('loaderDiv').style.visibility='hidden';
	  document.getElementById('loaderDiv').style.display='none';
	  document.getElementById('movie-grid').style.visibility='visible';
	  document.getElementById('movie-grid').style.display='block';
	  document.getElementById('search-field').style.display='block';
	  document.getElementById('search-field').style.visibility='visible';
	}
  
  dropZone.addEventListener('drop', function(evt){
  	evt.stopPropagation();
    evt.preventDefault();
    callDelay();
	 if (RAW_FILENAMES.length > 0)
		RAW_FILENAMES.length = 0;   
    processDroppedItems(evt.dataTransfer.items);
  }, false);
  // End Drag Drop Handlers


  var processDroppedItems = function(items) {
  	$.each(items, function(i, item) {
  		var entry = item.webkitGetAsEntry();
  		if(entry) parseFileTree(entry);
  	});

  	setTimeout(drawGrid, WAIT_FOR_PARSER_TO_COMPLETE);
  };

  function parseFileTree(entry) {
	  if (entry.isFile) {
      RAW_FILENAMES.push(entry.name);

    } else if(entry.isDirectory) {
      entry.createReader().readEntries(function(entries) {
      	$.each(entries, function(i, entry){
      		parseFileTree(entry);
      	});
	    }, function(e) { 
	    	console.error('Something went wrong when parsing directory.', e);
	    });
	  }
	}

  function drawGrid() {
		var movies = parseFilenamesToMovies(
			sanitizeFilenames(
				filterFilenames(RAW_FILENAMES)
			)
		);

    var poster_path = "images/noimage.gif"

		$.each(movies, function(i, movie){
			var $movieContainer = $(`
				<div id='m-container' class='browse-movie-wrap col-md-3 movie-col' data-uid='${uniqueTitle(movie.title)}' data-imdb-id=''>
					<a class='browse-movie-link'>
						<figure>
						<img id="img-responsive" class='figure animated fadeIn' src='${poster_path}' style="height:288px; width:198px;">
						<figcaption>
							<span class='imdb'><i class='fa fa-star' style='color: #f5de50;'></i></span>
							<h4 class='rating' data-rating=''></h4>
							<h5 class='genre'></h5>
							<span class='button-green-download-big'>View Details</span>
						</figcaption>
						</figure>
					</a>
					<div class='movie-bottom'>
						<a href='http://www.imdb.com/title/' class='browse-movie-title' target='_blank' data-title='${movie.title}' data-imdb-id=''>${movie.title}</a>
						<div class='movie-year' data-year='${movie.year}'>${movie.year}</div>
					</div>
				</div>
			`);



			$("#content.row").append($movieContainer);
			omdbApi(movie.title, movie.year, function(omdbResp){
				MOVIES[omdbResp.imdbID] = omdbResp;
				$movieContainer.find('.rating').text(omdbResp.imdbRating);
				$movieContainer.find('.genre').text(omdbResp.Genre.split(",").join(" | "));
        // $movieContainer.find('.browse-movie-link').attr('href', 'http://www.imdb.com/title/' + omdbResp.imdbID);
				$movieContainer.find('.browse-movie-title').attr('href', 'http://www.imdb.com/title/' + omdbResp.imdbID);
				// console.log(omdbResp.Year);
				$movieContainer.find('.movie-year').text(omdbResp.Year);
				$movieContainer.find('.browse-movie-title').text(omdbResp.Title);
				$movieContainer.find('.browse-movie-title').attr('data-imdb-id', omdbResp.imdbID);
				$movieContainer.attr('data-imdb-id', omdbResp.imdbID);
			});
		});

		// TMDB_URLS = createTmdbApiUrls(movies); // concat instead of set
		$.each(createTmdbApiUrls(movies), function(i,url){
			TMDB_URLS.push(url);
		});
  }

  function extractMovieFromTmdbResponse(tmdbResp){
  	if(tmdbResp.results)
  		return tmdbResp.results[0];
  	else return tmdbResp.movie_results[0];
  }

	function retryTmdbWithImdbIdFromOmdb(movie) {
		var $movieContainer = getMovieContainer(movie.title);
		var imdbId = $movieContainer.attr('data-imdb-id');

		if(typeof imdbId === 'string' && imdbId != '') {
			TMDB_URLS.push(`http://api.themoviedb.org/3/find/${imdbId}?external_source=imdb_id&api_key=${TMDB_API_KEY}`)
		} else setTimeout(function(){ retryTmdbWithImdbIdFromOmdb(movie); }, 1000);
	}

  function filterFilenames(rawFileNames) {
  	var mediaFileRegexp = /^.*\.(avi|mkv|wmv|flv|mpg|mp4|mov|divx|xvid)$/i;
  	var sample = /sample/i; 	
  	var filteredFileNames = [];

  	$.each(rawFileNames, function(i,rawFileName){
  		if(rawFileName.match(mediaFileRegexp)){
  			if(rawFileName.match(sample)){
  				// console.log("sample files excluded");
  			}
  			else {
  			  filteredFileNames.push(rawFileName);
  		  }
  		}
  	});

  	return filteredFileNames;
  }

  // Sanitize as to remove uploaded urls etc from file names.
  function sanitizeFilenames(rawFileNames) {
  	var prefixUrlRegexp = /^([a-z0-9]+(-[a-z0-9]+)*\.)+(com|to|cc|org|tf|tv)/i;
		var re = /\s(avi|mkv|wmv|flv|mpg|mp4|mov|divx|xvid)$/i;
		var ext = /.(avi|mkv|wmv|flv|mpg|mp4|mov|divx|xvid)$/i;	   	
  	var sanitizeedFileNames;

  	sanitizeedFileNames = $.map(rawFileNames, function(rawFileName){
  		return rawFileName.replace(prefixUrlRegexp, '').replace(ext, '').replace(re, '');
  	});

  	return sanitizeedFileNames;
  }

  function parseFilenamesToMovies(fileNames) {
  	return $.map(fileNames, ptn);
  }

  function uniqueTitle(title){
  	return title.replace(/([^0-9a-z]|\s+)/gi,'').toLowerCase();
  }

  function omdbApi(title, year, cb) {	
  	var url = `http://www.omdbapi.com/?apikey=${OMDB_API_KEY}&t=${title}&plot=short&r=json`;
  	if (typeof(year) !== 'undefined') url = url + `&y=${year}`
  	$.getJSON(url, cb);
  }

  function createTmdbApiUrls(movies) {
  	return $.map(movies, function(movie){
  		return `https://api.themoviedb.org/3/search/movie?query=${movie.title}&year=${movie.year}&year_optional=true&api_key=${TMDB_API_KEY}`;
  	});
  }


  function tmdbPosterUrl(path) {
  	return "http://image.tmdb.org/t/p/w185" + path;
  }

  function getMovieFromTmdbUrl(url) {
  	var imdbMatch = url.match(/(tt\d+)\?external_source=imdb_id/);
  	if(imdbMatch)
  		return { imdbId: imdbMatch[1] };

		var parts = url.replace("https://api.themoviedb.org/3/search/movie?query=",'').replace(/&year_optional.*$/,'').split('&year=');

		return {
			title: parts[0],
			year:  parseInt(parts[1])
		};
	}

  $.ajaxSetup({
	  beforeSend: function(jqXHR, settings) {
	      jqXHR.requestURL = settings.url;
	  }
	});

  var rateLimitedTmdbApiRunner = function(urls, requests, period, cb) {
	  if(urls.length == 0) {
	  	setTimeout(function() {
	      rateLimitedTmdbApiRunner(urls, requests, period, cb);
	    }, 2500);

	    return;
	  }
	  
	  var promises = [];

	  var then = new Date();
	  $.each(urls.splice(0, requests), function(index,url){
	    promises.push($.getJSON(url));
	  });

	  $.when.apply(this, promises).then(function(){
	  	var args = promises.length == 1 ? [arguments] : arguments;
	   	$.each(args, function(index, arg){ 
	    	if(typeof arg[0] == 'object')
		    	cb(arg[0], arg[2].requestURL); 
	    });

	    var now = new Date();
	    var timeTaken = (now - then) / 1000;
	    var wait = timeTaken < period ? period - timeTaken : 0
	      
	    setTimeout(function() {
	      rateLimitedTmdbApiRunner(urls, requests, period, cb);
	    }, wait * 1000);
	  });
	};

	function getMovieContainer(title) {
		// jQuery has a weird bug when using id and attibutes that start with number eg., #m-container[data-uid=12yearsaslave]
		// return $(`#m-container[data-uid=${uniqueTitle(title)}]`);

		// So lets just use class. :)
		return $(`.browse-movie-wrap[data-uid=${uniqueTitle(title)}]`);

	}

	function getMovieContainerByImdbId(imdbId) {
		return $(`#m-container[data-imdb-id=${imdbId}]`);	
	}

	rateLimitedTmdbApiRunner(TMDB_URLS, TMDB_CONCURRENT_REQUESTS, TMDB_CONCURRENT_REQUESTS_WINDOW_TIME, function(tmdbResp, requestURL) {
		var tmdbMovie = extractMovieFromTmdbResponse(tmdbResp);
		var requestedMovie = getMovieFromTmdbUrl(requestURL);

		if(typeof tmdbMovie === 'object') {
			var $movieContainer = requestedMovie.imdbId ? getMovieContainerByImdbId(requestedMovie.imdbId) : getMovieContainer(requestedMovie.title);
		  	$movieContainer.find('#img-responsive').attr('src', tmdbPosterUrl(tmdbMovie.poster_path));								
		} else retryTmdbWithImdbIdFromOmdb(requestedMovie);
	});

	var getModalTemplate = function(omdbMovie, poster) {
		return `
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: left;">${omdbMovie.Title} (${omdbMovie.Year})</h4>
		    </div>

				<div class="modal-body">
					<div class="row">
						<div class="col-lg-5">
			        <img style="width: 100%;" class="poster" srcset="${poster}">
						</div>

						<div class="col-lg-7" style="text-align: left;">
					    <div class="imdb_r">
				    		<div class="a">		
				    			<span>${omdbMovie.imdbRating}</span>
				    		</div>
					    	<div class="b">
					    		<div class="bar"><span style="width: ${omdbMovie.imdbRating * 10}%;"></span></div>
					    		<span class="dato"><b>IMDB: ${omdbMovie.imdbRating}/10</b></span>
					    	</div>
					    </div>										    
					    <p><strong>Genre:</strong>&nbsp;<span>${omdbMovie.Genre}</span></p>
					    <p><strong>Language:</strong>&nbsp;<span>${omdbMovie.Language}</span></p>
					    <p><strong>Director:</strong>&nbsp;<span>${omdbMovie.Director}</span></p>
					    <p><strong>Cast:</strong>&nbsp;<span>${omdbMovie.Actors}</span></p> 
					    <p>${omdbMovie.Plot}</p>
					    <p><a id="movie_209112" class="result" href="http://www.imdb.com/title/${omdbMovie.imdbID}" target='_blank'>More Info</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>`;
	};



	$(document).on('click', '.browse-movie-link', function(){
	  var imdbId = $(this).parent().attr("data-imdb-id");
	  var modal_poster = $(this).find("img").attr("src");
	  if(typeof MOVIES[imdbId] == 'object') {
      var omdbMovie = MOVIES[imdbId];
			$(getModalTemplate(omdbMovie, modal_poster)).modal('show');
	  } 
	});	



});