// start sorting alphabeticaly flip

      function defaultOthers(opt1,opt2){
        $(`.${opt1}-sortable`).removeClass('asc').removeClass('desc');
        $(`.${opt1}-sortable`).addClass('asc');
        $(`#${opt1}Sort`).removeClass('fa-arrow-up').removeClass('fa-arrow-down');
        $(`#${opt1}Sort`).addClass('fa-arrow-up');

        $(`.${opt2}-sortable`).removeClass('asc').removeClass('desc');
        $(`.${opt2}-sortable`).addClass('asc');
        $(`#${opt2}Sort`).removeClass('fa-arrow-up').removeClass('fa-arrow-down');
        $(`#${opt2}Sort`).addClass('fa-arrow-up');
      }
      jQuery(document).ready(function($){

        $('.alpha-sortable').on('click', function () {
          var option = $('.alpha-sortable').hasClass('asc') ? 'desc' : 'asc';
          var sortDir = $('#alphaSort').hasClass('fa-arrow-up') ? 'fa-arrow-down' : 'fa-arrow-up';
          var alphabeticallyOrderedDivs = $(".movie-col").sort(function (a, b) {

            // return option == 'asc'?$(a).find(".browse-movie-title").text().toLowerCase() < $(b).find(".browse-movie-title").text().toLowerCase() : 
            //        $(a).find(".browse-movie-title").text().toLowerCase() > $(b).find(".browse-movie-title").text().toLowerCase();
          var textA = $(a).find(".browse-movie-title").text().toLowerCase();
          var textB = $(b).find(".browse-movie-title").text().toLowerCase();
          if (option == 'desc'){
            if(textA < textB){
              return -1;
            }else if(textA > textB){
              return 1;
            }else{
              return 0;
            }
          }

          if (option == 'asc'){
            if(textA < textB){
              return 1;
            }else if(textA > textB){
              return -1;
            }else{
              return 0;
            }
          }             

          });
          $('.alpha-sortable').removeClass('asc').removeClass('desc');
          $('#alphaSort').removeClass('fa-arrow-up').removeClass('fa-arrow-down');
          $('#alphaSort').addClass(sortDir);
          $(this).addClass(option);
          $("#content").html(alphabeticallyOrderedDivs);
          //toggle others
          defaultOthers('num','year');
        });
      // end of sorting alphabeticaly flip

      // start sorting Rating flip
        var $divs = $(".movie-col");

        $('.num-sortable').on('click', function () {
            var option = $('.num-sortable').hasClass('asc') ? 'desc' : 'asc';
            var sortDir = $('#numSort').hasClass('fa-arrow-up') ? 'fa-arrow-down' : 'fa-arrow-up';
            var alphabeticallyOrderedDivs = $(".movie-col").sort(function (a, b) {
                // return option == 'asc' ? $(a).find(".rating").text() < $(b).find(".rating").text() : $(a).find(".rating").text() > $(b).find(".rating").text();
                numA = $(a).find(".rating").text();
                numB = $(b).find(".rating").text();
              if (option == 'desc'){
                if(numA < numB){
                  return -1;
                }else if(numA > numB){
                  return 1;
                }else{
                  return 0;
                }
              }

              if (option == 'asc'){
                if(numA < numB){
                  return 1;
                }else if(numA > numB){
                  return -1;
                }else{
                  return 0;
                }
              }
            });
            $('.num-sortable').removeClass('asc').removeClass('desc');
            $('#numSort').removeClass('fa-arrow-up').removeClass('fa-arrow-down');
            $('#numSort').addClass(sortDir);
            $(this).addClass(option);
            $("#content").html(alphabeticallyOrderedDivs);
            //toggle others
            defaultOthers('alpha','year');
        });
      // end of sorting Rating Asc

      // start sorting year flip
        $('.year-sortable').on('click', function () {
            var option = $('.year-sortable').hasClass('asc') ? 'desc' : 'asc';
            var sortDir = $('#yearSort').hasClass('fa-arrow-up') ? 'fa-arrow-down' : 'fa-arrow-up';
            var alphabeticallyOrderedDivs = $(".movie-col").sort(function (a, b) {
                // return option == 'asc' ? $(a).find(".movie-year").text() < $(b).find(".movie-year").text() : $(a).find(".movie-year").text() > $(b).find(".movie-year").text();
                yearA = $(a).find(".movie-year").text();
                yearB = $(b).find(".movie-year").text();
                if (option == 'desc'){
                  if(yearA < yearB){
                    return -1;
                  }else if(yearA > yearB){
                    return 1;
                  }else{
                    return 0;
                  }
                }

                if (option == 'asc'){
                  if(yearA < yearB){
                    return 1;
                  }else if(yearA > yearB){
                    return -1;
                  }else{
                    return 0;
                  }
                }
            });
            $('.year-sortable').removeClass('asc').removeClass('desc');
            $('#yearSort').removeClass('fa-arrow-up').removeClass('fa-arrow-down');
            $('#yearSort').addClass(sortDir);
            $(this).addClass(option);
            $("#content").html(alphabeticallyOrderedDivs);
            //toggle others
            defaultOthers('num','alpha');
        });
      });
      // end of sorting year flip