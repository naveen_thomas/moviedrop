//start multi-select
  $(document).ready(function() {

    
    
    $('#genre-multi-select').multiselect({
      onChange: function(option, checked) {

        $('.movie-col h5').each(function(){
          var genreString = $(this).text().replace(/\s/g,"").replace(/-/g,"").toLowerCase();
          var filterTerm = genreString.split("|");
          $(this).attr('filter-term', filterTerm);
        });
        
        $('.movie-col h5').each(function(i,el){
          $(el).attr('filter-flag',false);
        });

        var searchTerm = new Array();
        $('#genre-multi-select option:selected').each(function(i,el){
          searchTerm.push($(el).text().replace(/-/g,"").toLowerCase());//Array of multi-select genres
        });

        $('.movie-col h5').each(function(i,el){
          var dataTerms = $(el).attr('filter-term').split(",");
          $.each(searchTerm, function(i,val) {
            if ($.inArray(val, dataTerms) != -1) {
                $(el).attr('filter-flag',true);
            }
          });
        });

        $('.movie-col h5').each(function(i,el){
          if(searchTerm.length > 0){
            if($(el).attr('filter-flag') == 'true'){
              $(el).parents('.movie-col').show();
            }else{
              $(el).parents('.movie-col').hide();
            }
          }else{
            $(el).parents('.movie-col').show();
          }
        });
        
      }//end of onchange
    });//end multi-select
  });// end of document